A simple task manager based on streams.

Compatibility
-------------

**Python**: 3.8

Docker
------

Docker-compose >=1.28 is supported. There are two docker-compose sample files: the main config file
and the pytest file. Pytest file is expected to be used by app tests and has non-persistent containers.

To run the main file environment and build the app use `full` profile:

.. code-block::

  docker-compose --profile full up

You can customize your environment by adding different profiles to the compose file.

Testing
-------

At first, you must install `requirements.tests.txt` as well
as normal requirements. You also will need docker and (probably) docker-compose
to run all tests since they usually require a fresh environment.

pytest
^^^^^^

Run `pytest` command. There's also a Pycharm *unittests*
run configuration ready to use.

tox
^^^

To test with tox you should install and use `pyenv`. First
setup local interpreters which you want to use in tests.

```pyenv local 3.7.5 3.8.1 3.9.0```

Then you can run `tox` command to test against all of them.

Documentation
-------------

sphinx
^^^^^^

Install `requirements.docs.txt` and then
cd to `./docs` and run `make html` command. There is also a
run configuration for Pycharm.
