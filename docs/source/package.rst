APP
=======

main
----

.. automodule:: app.__main__
   :members:
   :undoc-members:
   :show-inheritance:

application
-----------

.. automodule:: app.application
   :members:
   :undoc-members:
   :show-inheritance:

CLI
---

.. automodule:: app.CLI
   :members:
   :undoc-members:
   :show-inheritance:

loop
----

.. automodule:: app.loop
   :members:
   :undoc-members:
   :show-inheritance:

routes
------

.. automodule:: app.routes
   :members:
   :undoc-members:
   :show-inheritance:

service_registry
----------------

.. automodule:: app.service_registry
   :members:
   :undoc-members:
   :show-inheritance:

setting_service
---------------

.. automodule:: app.setting_service
   :members:
   :undoc-members:
   :show-inheritance:
