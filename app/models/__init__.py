from kaiju_db.services import functions_registry

from .tables import *
from .functions import *

functions_registry.register_classes_from_namespace(globals())
