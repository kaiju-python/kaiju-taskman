"""
This block registers all the service classes to be discovered later by
a class manager.
"""

import kaiju_tools
from kaiju_tools.services import service_class_registry

import app.services

service_class_registry.register_classes_from_module(kaiju_tools)
service_class_registry.register_classes_from_module(app.services)
