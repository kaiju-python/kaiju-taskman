import asyncio
import logging
import random
import uuid

from kaiju_tools.exceptions import APIException
from kaiju_tools.services import Service
from kaiju_tools.rpc import AbstractRPCCompatible

__all__ = ['TestService']


class TestService(Service, AbstractRPCCompatible):
    """A service for testing purposes."""

    service_name = 'test'

    @property
    def routes(self):
        return {
            'io.print': self.io_print,
            'io.log': self.io_log,
            'io.echo': self.io_echo
        }

    @property
    def permissions(self) -> dict:
        return {
            '*': self.PermissionKeys.GLOBAL_SYSTEM_PERMISSION
        }

    async def io_print(self, text: str):
        """Print text to the console."""
        self._check_debug()
        print(text)

    async def io_log(self, text: str, loglevel='INFO'):
        """Print text to the service log."""
        self._check_debug()
        self.logger.log(getattr(logging, loglevel, 10), text)

    async def io_echo(self, **kws):
        """Echo all arguments back to the client."""
        self._check_debug()
        return kws

    def _check_debug(self):
        if not self.app.debug:
            raise APIException('Test services are only available in debug mode.')
