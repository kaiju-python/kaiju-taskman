import pytest

from kaiju_tools.docker import *
from kaiju_tools.tests.fixtures import *
#from kaiju_db.tests.fixtures import *


@pytest.fixture
def app_stack(application, logger):
    """All services including the app itself."""
    with DockerStack(
        name='pytest-stack',
        compose_files=['./docker-compose.yaml', './docker-compose-pytest.yaml'],
        profiles=['full'],
        build=True,
        app=application(),
        logger=logger
    ) as stack:
        yield stack
